# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AboutDialog(object):
    def setupUi(self, AboutDialog):
        AboutDialog.setObjectName("AboutDialog")
        AboutDialog.resize(487, 505)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(AboutDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(AboutDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(AboutDialog)
        self.label_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_2.setLineWidth(1)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.label_3 = QtWidgets.QLabel(AboutDialog)
        self.label_3.setScaledContents(False)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.label_4 = QtWidgets.QLabel(AboutDialog)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.label_5 = QtWidgets.QLabel(AboutDialog)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.pushButton = QtWidgets.QPushButton(AboutDialog)
        self.pushButton.setCheckable(False)
        self.pushButton.setChecked(False)
        self.pushButton.setDefault(True)
        self.pushButton.setFlat(False)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_2.addWidget(self.pushButton)

        self.retranslateUi(AboutDialog)
        self.pushButton.clicked.connect(AboutDialog.close)
        QtCore.QMetaObject.connectSlotsByName(AboutDialog)

    def retranslateUi(self, AboutDialog):
        _translate = QtCore.QCoreApplication.translate
        AboutDialog.setWindowTitle(_translate("AboutDialog", "Dialog"))
        self.label.setText(_translate("AboutDialog", "<b>VocalDilate</b>"))
        self.label_2.setText(_translate("AboutDialog", "Proudly brought to you by zeograd <a href=\"mailto:olivier@pcedev.com\">&lt;olivier@pcedev.com&gt;</a>"))
        self.label_3.setText(_translate("AboutDialog", "<html><head/><body><p>This software modifies wav files to align them on a reference wav file.</p><p>Its use case is to tighten vocal tracks meant to be played together (ie remove smearing and timing misalignements due to interpretation differences).</p><p>First pick one vocal track as reference (one which is on time and with a clear pronunciation) and add one or more target tracks (which will be modified to fit the reference one). You can then click on &quot;Dilate!&quot; to perform the computation.</p><p>Resulting files are created next to the original target files with a different extension.<br/>They are roughly volume normalized to fit the reference.</p><p>&quot;*-silence-align.wav&quot; files are only modified by aligning silences and &quot;*-passX.wav&quot; are generated after successive passes at aligning sound power between reference and target.</p><p>Accepted input format is WAV only, preferably with a RMS volume in the -12 ~ -6 dB range.</p></body></html>"))
        self.label_4.setText(_translate("AboutDialog", "License: GNU GPL"))
        self.label_5.setText(_translate("AboutDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Links:\n"
"<ul><li><a href=\"https://gitlab.com/zeograd/vocalstitch\">Source code</a>\n"
"<li><a href=\"https://pcedev.com\">Author homepage</a></ul></p></body></html>"))
        self.pushButton.setText(_translate("AboutDialog", "OK"))

