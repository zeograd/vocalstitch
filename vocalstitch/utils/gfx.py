import logging

try:
    import matplotlib.pyplot as plt

    clf = plt.clf
    show = plt.show
    plot = plt.plot
except ImportError:
    def nop(*args, **kwargs):
        pass


    clf = show = plot = nop


def draw_rms(filename, rms, comparison_rms=None):
    try:
        from PIL import Image, ImageDraw
    except ImportError:
        logging.warn("Drawing disabled, install pillow to enable this feature")
        return

    import numpy as np

    image_width = min(len(rms), 800)
    image_height = 200

    color_1 = (255, 0, 0)  # red
    color_2 = (255, 255, 255)  # white
    color_3 = (255, 255, 0)  # yellow

    image = Image.new("RGB", (image_width, image_height))
    draw = ImageDraw.Draw(image)

    for x in range(image_width):
        try:
            y = np.max(rms[int(x / image_width * len(rms)): int((x + 1) / image_width * len(rms))])
            if y > -image_height / 2:
                draw.line((x, -y, x, image_height + y), color_1)
            else:
                draw.point((x, image_height / 2), color_1)

            y = np.mean(rms[int(x / image_width * len(rms)):int((x + 1) / image_width * len(rms))])
            if y > -image_height / 2:
                draw.line((x, - y, x, image_height + y), color_2)
            else:
                draw.point((x, image_height / 2), color_2)

            if comparison_rms is not None:
                comparison_fragment = comparison_rms[
                                      int(x / image_width * len(rms)):int((x + 1) / image_width * len(rms))]
                if len(comparison_fragment) > 0:
                    y = np.mean(comparison_fragment)
                    if y > -image_height / 2:
                        draw.point((x, - y), color_3)
                    else:
                        draw.point((x, image_height / 2), color_3)
        except ValueError:
            pass

    image.save(filename)

    del draw
