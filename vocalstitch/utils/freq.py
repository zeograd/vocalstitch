import numpy as np

from vocalstitch.utils import librosafft


def get_delta_rms(lhs, rhs):
    if len(lhs) > len(rhs):
        rhs = np.pad(rhs, (0, len(lhs) - len(rhs)), 'constant', constant_values=(-80,))
    elif len(rhs) > len(lhs):
        lhs = np.pad(lhs, (0, len(rhs) - len(lhs)), 'constant', constant_values=(-80,))
    return np.sqrt(np.mean(np.square(rhs - lhs)))


def band_filter(data, rate, low_freq, hi_freq):
    hop_length = 512

    # Construct the stft
    stft = librosafft.stft(data, hop_length=hop_length)

    # compute indexes for low and high frequencies
    index_low_freq = int(low_freq / (rate / 2 / stft.shape[0]))
    index_hi_freq = int(hi_freq / (rate / 2 / stft.shape[0]))

    # cut frequencies outside band
    stft[:index_low_freq - 1] = 0
    stft[index_hi_freq + 1:] = 0

    # stft_stretch = librosafft.phase_vocoder(stft)

    # Invert the stft
    return librosafft.istft(stft, hop_length=hop_length, dtype=data.dtype)


def detect_main_freq(data, rate, low_freq, hi_freq, n_fft=2048, hop_length=512):
    # Construct the stft
    stft = librosafft.stft(data, n_fft=n_fft, hop_length=hop_length)

    # compute indexes for low and high frequencies
    index_low_freq = int(low_freq / (rate / 2 / stft.shape[0]))
    index_hi_freq = int(hi_freq / (rate / 2 / stft.shape[0]))

    # cut frequencies outside band
    stft[:index_low_freq - 1] = 0
    stft[index_hi_freq + 1:] = 0

    # wave time (in s) = stft index * hop_length / rate

    rms = []

    for t in range(stft.shape[1]):
        # rms.append(10*np.log(np.sum(np.abs(stft.T[t]))))
        magnitude = np.abs(stft.T[t])
        strongest_freq = np.argmax(magnitude)
        strongest_freq_intensity = magnitude[strongest_freq]

        strongest_freq *= (rate / 2. / stft.shape[0])

        strongest_freq_intensity = np.log(strongest_freq_intensity / stft.shape[0])
        strongest_freq_intensity *= 10

        if strongest_freq_intensity <= -90:
            strongest_freq = 0

        print("{0:.05} {1:3} {2}".format(t * hop_length / rate, int(strongest_freq), int(strongest_freq_intensity)))

    return None
