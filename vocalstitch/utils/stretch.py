import logging
import os
import subprocess
import tempfile

import numpy as np
from vocalstitch.utils import freq, librosafft
from vocalstitch.utils import wavfile

FFT_SIZE = 2 * 1024

# MAX_DEFORM = 0.1
# DEFORM_POWER = 1.2
# DEFORM_STEPS = 20
MAX_DEFORM = 0.05
DEFORM_POWER = 1.2
DEFORM_STEPS = 10


def interpolate_rms(wav_rms, time_steps):
    if len(time_steps) != len(wav_rms):
        raise ValueError('Different length for wav_rms and time_steps')
    return np.interp(time_steps, np.arange(0, len(wav_rms)), wav_rms)


def adapt_timesteps_power(time_steps, param):
    steps = np.linspace(0, 1, len(time_steps))
    return np.interp(np.power(steps, param), steps, time_steps)


def deform_rms(wav_rms, reference_filtered_rms, time_steps_ms, min_index, max_index, depth=0, max_depth=None):
    # if the interval to work on is too small, ignore attempt to deform wav
    if max_index - min_index < 10 or max_depth is not None and depth >= max_depth:
        return

    best_deform = 1
    best_matching = freq.get_delta_rms(
        interpolate_rms(wav_rms, time_steps_ms),
        reference_filtered_rms)

    for deform in np.power(
        np.linspace(np.power(1 - MAX_DEFORM, 1 / DEFORM_POWER), np.power(1 + MAX_DEFORM, 1 / DEFORM_POWER),
                    DEFORM_STEPS), DEFORM_POWER):

        local_time_steps = time_steps_ms.copy()
        local_time_steps[int(min_index):int(max_index)] = adapt_timesteps_power(
            local_time_steps[int(min_index):int(max_index)], deform)

        matching = freq.get_delta_rms(
            interpolate_rms(wav_rms, local_time_steps),
            reference_filtered_rms)

        if matching < best_matching:
            best_matching = matching
            best_deform = deform

    # retains the best deformation
    time_steps_ms[int(min_index):int(max_index)] = adapt_timesteps_power(time_steps_ms[int(min_index):int(max_index)],
                                                                         best_deform)

    if not np.all(np.diff(time_steps_ms) > 0):
        print("space/time error !!!")

    # print("best matching: {}".format(best_matching))

    deform_rms(wav_rms, reference_filtered_rms, time_steps_ms, min_index, (max_index + min_index) / 2, depth + 1,
               max_depth=max_depth)
    deform_rms(wav_rms, reference_filtered_rms, time_steps_ms, (max_index + min_index) / 2, max_index, depth + 1,
               max_depth=max_depth)

    if not np.all(np.diff(time_steps_ms) > 0):
        print("space/time error !!!")


def stft_stretch(input=None, time_steps=None, stft=None, time_steps_ms=None):
    hop_length = 512

    # Construct the stft
    if stft is None:
        stft = librosafft.stft(input, FFT_SIZE, hop_length=hop_length)

    if time_steps_ms is not None and time_steps is None:
        time_steps = np.zeros(stft.shape[1])
        ratio = len(time_steps_ms) / len(time_steps)

        for t in range(len(time_steps)):
            time_steps[t] = np.mean(
                (time_steps_ms / len(time_steps_ms) * stft.shape[1])[(int)(t * ratio): int((t + 1) * ratio)])

    stft_stretch = librosafft.phase_vocoder(stft, hop_length=hop_length, time_steps=time_steps)

    # Invert the stft
    return librosafft.istft(stft_stretch, hop_length=hop_length, dtype=input.dtype)


rubberband_found = None


def detect_rubberband():
    global rubberband_found
    if rubberband_found is not None:
        return rubberband_found

    try:
        subprocess.check_output(["rubberband", "-V"], stderr=subprocess.STDOUT, shell=(os.name == 'nt'))
        rubberband_found = True
    except (FileNotFoundError, subprocess.CalledProcessError):
        import sys
        logging.warning(
            "Rubberband is not found, reverting to suboptimal internal STFT for stretching (search path=%s, cwd=%s)",
            sys.path, os.getcwd())
        rubberband_found = False


def rubberband_sparse_stretch(wav_data, sync_points, wav_rate):
    try:
        map_file = tempfile.NamedTemporaryFile(delete=False)
        input_file = tempfile.NamedTemporaryFile(delete=False)
        output_file = tempfile.NamedTemporaryFile(delete=False)

        with map_file.file as f:
            for x, y in sync_points:
                f.write("{0} {1}\n".format(int(y * wav_rate / 1000.), int(x * wav_rate / 1000.)).encode())

        input_file.close()
        wavfile.write(input_file.name, wav_rate, wav_data)

        output_file.close()

        logging.debug(subprocess.check_output(
            ["rubberband", "-t1", "--timemap", map_file.name, "-c6", input_file.name, output_file.name],
            stderr=subprocess.STDOUT, shell=(os.name == 'nt')))

        wav_rate, wav_data = wavfile.read(output_file.name)

        return wav_data

    finally:
        os.remove(input_file.name)
        os.remove(output_file.name)
        os.remove(map_file.name)


def rubberband_stretch(wav_data, time_steps_ms, wav_rate):
    try:
        map_file = tempfile.NamedTemporaryFile(delete=False)
        input_file = tempfile.NamedTemporaryFile(delete=False)
        output_file = tempfile.NamedTemporaryFile(delete=False)

        with map_file.file as f:
            for source_sync_point, target_sync_point in enumerate(time_steps_ms):
                f.write("{0} {1}\n".format(int(target_sync_point * wav_rate / 1000.),
                                           int(source_sync_point * wav_rate / 1000.)).encode())

        input_file.close()
        wavfile.write(input_file.name, wav_rate, wav_data)

        output_file.close()

        logging.debug(subprocess.check_output(
            ["rubberband", "-t1", "--timemap", map_file.name, "-c6", input_file.name, output_file.name],
            stderr=subprocess.STDOUT, shell=(os.name == 'nt')))

        wav_rate, wav_data = wavfile.read(output_file.name)

        return wav_data

    finally:
        os.remove(input_file.name)
        os.remove(output_file.name)
        os.remove(map_file.name)


def stretch_wav(wav_data, time_steps_ms, wav_rate):
    if detect_rubberband():
        return rubberband_stretch(wav_data, time_steps_ms, wav_rate)
    return stft_stretch(wav_data, time_steps_ms=time_steps_ms)


def slide_smear(time_steps_ms, source, target, target_rms):
    padding = int(.5 * (target[1] - target[0]))
    fixed_x_coordinates = np.min([source[0], target[0]]) - padding, np.max([source[1], target[1]]) + padding
    source_copy = time_steps_ms.copy()

    if np.max(target_rms[fixed_x_coordinates[0]: target[0]]) > -24:
        time_steps_ms[fixed_x_coordinates[0]: target[0]] = lininterp(fixed_x_coordinates[0], target[0],
                                                                     fixed_x_coordinates[0], source[0],
                                                                     source_copy)

    time_steps_ms[target[0]:target[1]] = lininterp(target[0], target[1],
                                                   source[0], source[1],
                                                   source_copy)

    if np.max(target_rms[target[1]: fixed_x_coordinates[1]]) > -24:
        time_steps_ms[target[1]: fixed_x_coordinates[1]] = lininterp(target[1], fixed_x_coordinates[1],
                                                                     source[1], fixed_x_coordinates[1],
                                                                     source_copy)


        # time_steps_ms[fixed_x_coordinates[0]:fixed_x_coordinates[1] + padding] = np.interp(
        #     [fixed_x_coordinates[0], target[0], target[1], fixed_x_coordinates[1]],
        #     [fixed_x_coordinates[0], source[0], source[1], fixed_x_coordinates[1]],
        #     [time_steps_ms[fixed_x_coordinates[0]], time_steps_ms[source[0]], time_steps_ms[source[1]],
        #      time_steps_ms[fixed_x_coordinates[1]]])


def lininterp(target_start, target_end, source_start, source_end, time_steps_ms):
    return time_steps_ms[source_start] + np.arange(target_end - target_start) / (target_end - target_start) * (
        time_steps_ms[source_end] - time_steps_ms[source_start])


def search_delta(delta):
    result = []
    x = 0
    while x < len(delta):

        if delta[x]:
            # found start of delta

            delta_start = x

            while x < len(delta) and delta[x]:
                x += 1

            delta_end = x

            result.append((delta_start, delta_end))

            print("found delta from {0} to {1}".format(delta_start, delta_end))
        else:
            x += 1

    return result


def reduct_smearing(filtered_rms, reference_filtered_rms, time_steps_ms):
    quantized_positive_delta = reference_filtered_rms - filtered_rms > 0.5 * np.max(
        reference_filtered_rms - filtered_rms)
    quantized_negative_delta = reference_filtered_rms - filtered_rms < 0.5 * np.min(
        reference_filtered_rms - filtered_rms)

    positive_deltas = search_delta(quantized_positive_delta)
    negative_deltas = search_delta(quantized_negative_delta)

    for d in positive_deltas:
        slide_smear(time_steps_ms, (2 * d[0] - d[1], d[0]), d, filtered_rms)
    # for d in negative_deltas:
    #     slide_smear(time_steps_ms, d, (2 * d[0] - d[1], d[0]), filtered_rms)

    return None

    # plt.clf()
    # plt.plot(reference_filtered_rms, color="red")
    # plt.plot(filtered_rms, color="yellow")
    # plt.plot(reference_filtered_rms-filtered_rms, color="green")


def get_silences(normalized_rms, quiet_min_duration=50, quiet_detection_threshold=-18):
    silence_start = None
    silences = []

    for i in range(normalized_rms.shape[0]):

        if normalized_rms[i] < quiet_detection_threshold:
            if silence_start is not None:
                continue
            else:
                silence_start = i
        else:
            if silence_start is not None:
                silences.append((silence_start, i - 1))
                silence_start = None
            else:
                continue

    if silence_start is not None:
        silences.append((silence_start, normalized_rms.shape[0]))

    return [s for s in silences if s[1] - s[0] > quiet_min_duration]


def silence_align(wav_data, wav_rate, reference_data, reference_rate, time_step_ms, quiet_matching_distance=300,
                  min_freq=200, max_freq=800, quiet_min_duration=50, quiet_detection_threshold=-18):

    from vocalstitch.dilatevocal import get_normalized_rms

    reference_filtered_data = freq.band_filter(reference_data, reference_rate, min_freq, max_freq)
    reference_normalized_rms = get_normalized_rms(reference_filtered_data, reference_rate)

    filtered_data = freq.band_filter(wav_data, wav_rate, min_freq, max_freq)
    normalized_rms = get_normalized_rms(filtered_data, wav_rate)

    # detect quiets
    reference_quiets = get_silences(reference_normalized_rms, quiet_min_duration=quiet_min_duration,
                                    quiet_detection_threshold=quiet_detection_threshold)
    quiets = get_silences(normalized_rms, quiet_min_duration=quiet_min_duration,
                          quiet_detection_threshold=quiet_detection_threshold)

    # add first silence sync point only if within the matching distance
    if - quiet_matching_distance <= reference_quiets[0][1] - quiets[0][1] <= quiet_matching_distance:
        sync_points = [(reference_quiets[0][1], quiets[0][1])]
    else:
        sync_points = []

    reference_quiet_idx = 1
    quiet_idx = 1

    while reference_quiet_idx < len(reference_quiets) - 1:

        # discard old quiets
        while quiets[quiet_idx][0] < reference_quiets[reference_quiet_idx][
            0] - quiet_matching_distance and quiet_idx < len(quiets) - 1:
            quiet_idx += 1

        if - quiet_matching_distance <= reference_quiets[reference_quiet_idx][0] - quiets[quiet_idx][
            0] <= quiet_matching_distance:
            sync_points.append((reference_quiets[reference_quiet_idx][0], quiets[quiet_idx][0]))

        # discard old quiets
        while quiets[quiet_idx][1] < reference_quiets[reference_quiet_idx][
            1] - quiet_matching_distance and quiet_idx < len(quiets) - 1:
            quiet_idx += 1

        if - quiet_matching_distance <= reference_quiets[reference_quiet_idx][1] - quiets[quiet_idx][
            1] <= quiet_matching_distance:
            sync_points.append((reference_quiets[reference_quiet_idx][1], quiets[quiet_idx][1]))

        reference_quiet_idx += 1

    # add last silence sync point only if within the matching distance
    if -quiet_matching_distance <= reference_quiets[-1][0] - quiets[-1][0] <= quiet_matching_distance:
        sync_points.append((reference_quiets[-1][0], quiets[-1][0]))

    # complete sync point with the very first and very last sample to allow interpolate to work
    sync_points = [(0, 0)] + sync_points + [(reference_quiets[-1][1], quiets[-1][1])]

    time_step_ms = np.interp(time_step_ms, [s[0] for s in sync_points], [s[1] for s in sync_points])

    return sync_points


def normalize_rms(wav_data, target_rms):
    from vocalstitch.dilatevocal import get_overall_rms
    origin_rms = get_overall_rms(wav_data)

    maximum_rms_factor = 1. / np.max(np.abs(wav_data))
    factor = min(maximum_rms_factor, origin_rms / target_rms)

    logging.info("Normalizing result file by %d%%", 100. * (factor - 1))

    return wav_data * factor
