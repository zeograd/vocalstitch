from __future__ import print_function

import argparse
import logging
import sys

import numpy as np
import os

from vocalstitch.utils import wavfile, freq, gfx, stretch

__version__ = '0.9'
__created__ = '2016-05-17'
__updated__ = '2016-06-15'

ANALYSIS_POWER_CURVE = 0
ANALYSIS_SMEARING_REDUCTION = 1


def get_wave(wav_data=None, wav_rate=None, filename=None):
    if wav_data is None and wav_rate is None and filename is None:
        raise ValueError("(wav_data, wav_rate) or filename must be filled ")

    if wav_data is None and wav_rate is None:
        wav_rate, wav_data = wavfile.read(filename)

        # normalize to [ -1 : +1 ] when reading file, assume it already is when in memory
        wav_data = wav_data / float(2 ** (8 * wav_data.dtype.itemsize - 1))

    if len(wav_data.shape) > 1:  # convert to mono
        wav_data = (wav_data[:, 0] + wav_data[:, 1]) * 0.5

    return wav_rate, wav_data


def get_overall_rms(wav_data):
    return 10 * np.log(np.sqrt(np.mean(np.square(wav_data))))


def get_rms(wav_data=None, wav_rate=None, filename=None):
    wav_rate, wav_data = get_wave(wav_data, wav_rate, filename)

    wav_data = np.square(wav_data.copy())

    step = wav_rate * .001  # 1 ms step
    window = int(wav_rate * .02)  # 20ms windows

    wav_index = 0

    result = np.empty(np.math.ceil(len(wav_data) / step))
    result_index = 0

    if filename is not None:
        logging.info("Processing %s", filename)

    while wav_index < len(wav_data):
        data = wav_data[int(wav_index):int(wav_index) + window]
        wav_index += step

        # result[result_index] = max(-80, 10 * np.log(np.sqrt(np.mean(np.square(data)))))
        result[result_index] = 10 * np.log(np.sqrt(np.mean(np.square(data))))
        result_index += 1

    return result


def get_normalized_rms(wav_data=None, wav_rate=None, filename=None, target_median_rms=-12, silence_threshold=-24):
    rms = get_rms(wav_data, wav_rate, filename)
    rms /= np.median(rms) / target_median_rms
    return np.maximum(silence_threshold, rms)


def get_matching(rms_reference, rms, delta=0):
    lhs_mean = np.mean(rms_reference)
    rhs_mean = np.mean(rms)

    matching = 0

    for index in range(len(rms_reference)):
        lhs_rms = rms_reference[index]
        try:
            rhs_rms = rms[index + delta]
        except IndexError:
            rhs_rms = -100

        if lhs_rms < lhs_mean and rhs_rms < rhs_mean:
            matching += 1
        elif lhs_rms > lhs_mean and rhs_rms > rhs_mean:
            matching += 1

    return matching / len(rms_reference)


def get_best_offset(rms_reference, rms):
    best_matching = None
    best_delta = None

    min_range = -400
    max_range = 400

    for delta in range(min_range, max_range):

        if delta % 5 == 0:
            print("{0}{1:5}ms ".format('' if delta % 50 != 0 and delta > min_range else '\n', delta), end='')

        sys.stdout.flush()

        matching = get_matching(rms_reference, rms, delta)

        logging.debug("delta %d, matching %f", delta, matching)

        if best_matching is None or matching > best_matching:
            best_matching = matching
            best_delta = delta
            print("X", end='')
        else:
            print(".", end='')

        sys.stdout.flush()

    print()

    return best_delta


def dilate_wavfiles(reference_filename, target_filenames, visual_debug=False, max_pass=3, output_directory=None,
                    analysis_method=ANALYSIS_POWER_CURVE, min_freq=200, max_freq=800,
                    target_median_rms=-12, silence_threshold=-24, quiet_matching_distance=300, quiet_min_duration=50,
                    quiet_detection_threshold=-16, power_curve_max_depth=8,
                    **opts):
    logging.info("Loading reference file: %s", reference_filename)

    reference_rate, reference_data = get_wave(filename=reference_filename)

    # reference_rms = get_normalized_rms(wav_data=reference_data, wav_rate=reference_rate,
    #                                    target_median_rms=target_median_rms, silence_threshold=silence_threshold)
    reference_overall_rms = get_overall_rms(reference_data)

    reference_filtered_rms = get_normalized_rms(
        wav_data=freq.band_filter(reference_data, reference_rate, min_freq, max_freq),
        wav_rate=reference_rate, target_median_rms=target_median_rms, silence_threshold=silence_threshold)

    if visual_debug:
        gfx.draw_rms("filtered.png", reference_filtered_rms)

    for wav_filename in target_filenames:

        logging.info("Loading target file: %s", wav_filename)

        wav_rate, wav_data = get_wave(filename=wav_filename)

        # rms = get_normalized_rms(wav_data=wav_data, wav_rate=wav_rate)
        # overall_offset = get_best_offset(reference_rms, rms)
        # print("best overall offset for {}: {} ms".format(wav_filename, overall_offset))

        filtered_rms = get_normalized_rms(
            wav_data=freq.band_filter(wav_data, wav_rate, min_freq, max_freq),
            wav_rate=wav_rate,
            target_median_rms=target_median_rms,
            silence_threshold=silence_threshold)

        if visual_debug:
            gfx.draw_rms("{0}.original.png".format(wav_filename), filtered_rms)

        # default time steps, no deformation
        time_steps_ms = np.arange(0, len(filtered_rms), dtype=np.float)

        # prepare output filename
        if output_directory is not None:
            output_filename = os.path.join(output_directory, os.path.basename(wav_filename))
        else:
            output_filename = wav_filename

        # strip extension
        output_filename = os.path.splitext(output_filename)[0]

        #  debug difference
        logging.debug(
            "delta rms with 0% stretching: {}".format(freq.get_delta_rms(reference_filtered_rms, get_normalized_rms(
                wav_data=freq.band_filter(stretch.stretch_wav(wav_data, time_steps_ms, wav_rate), wav_rate,
                                          min_freq,
                                          max_freq),
                wav_rate=wav_rate, target_median_rms=target_median_rms, silence_threshold=silence_threshold))))

        initial_difference = freq.get_delta_rms(reference_filtered_rms, get_normalized_rms(
            wav_data=freq.band_filter(wav_data, wav_rate, min_freq, max_freq),
            wav_rate=wav_rate, target_median_rms=target_median_rms, silence_threshold=silence_threshold))

        logging.info("Initial difference with reference: %.3g", initial_difference)

        # align silences first
        sync_points = stretch.silence_align(wav_data, wav_rate, reference_data, reference_rate, time_steps_ms,
                                            quiet_matching_distance=quiet_matching_distance, min_freq=min_freq,
                                            max_freq=max_freq, quiet_min_duration=quiet_min_duration,
                                            quiet_detection_threshold=quiet_detection_threshold)
        wavfile.write("{0}-silence-aligned.wav".format(output_filename), wav_rate,
                      stretch.normalize_rms(stretch.rubberband_sparse_stretch(wav_data, sync_points, wav_rate),
                                            reference_overall_rms))

        logging.debug(
            "delta rms with silence aligning: {}".format(freq.get_delta_rms(reference_filtered_rms, get_normalized_rms(
                wav_data=freq.band_filter(stretch.stretch_wav(wav_data, time_steps_ms, wav_rate), wav_rate,
                                          min_freq,
                                          max_freq),
                wav_rate=wav_rate, target_median_rms=target_median_rms, silence_threshold=silence_threshold))))

        for pass_idx in range(max_pass):

            logging.info("Dilating %s (pass %d/%d)", wav_filename, pass_idx + 1, max_pass)

            if analysis_method == ANALYSIS_POWER_CURVE:
                # detect optimal time steps
                stretch.deform_rms(filtered_rms, reference_filtered_rms, time_steps_ms, 0, len(time_steps_ms),
                                   max_depth=power_curve_max_depth)
            elif analysis_method == ANALYSIS_SMEARING_REDUCTION:
                stretch.reduct_smearing(filtered_rms, reference_filtered_rms, time_steps_ms)

            if visual_debug:
                gfx.clf()
                gfx.plot(reference_filtered_rms, color='red')
                gfx.plot(filtered_rms, color='orange')
                gfx.plot(time_steps_ms, filtered_rms, color='blue')
                gfx.show()

            optimal_wav_data = stretch.stretch_wav(wav_data, time_steps_ms, wav_rate)

            # reevaluate filtered rms when using smearing reduction
            if analysis_method == ANALYSIS_SMEARING_REDUCTION:
                filtered_rms = get_normalized_rms(
                    wav_data=freq.band_filter(optimal_wav_data, wav_rate, min_freq, max_freq),
                    wav_rate=wav_rate,
                    target_median_rms=target_median_rms,
                    silence_threshold=silence_threshold)

            current_difference = freq.get_delta_rms(reference_filtered_rms, get_normalized_rms(
                wav_data=freq.band_filter(optimal_wav_data, wav_rate, min_freq, max_freq),
                wav_rate=wav_rate, target_median_rms=target_median_rms, silence_threshold=silence_threshold))

            logging.info("Difference with reference after pass %d: %.3g", pass_idx, current_difference)

            # if visual_debug:
            #     gfx.draw_rms("{0}.comparison{1}.png".format(wav_filename, pass_idx), reference_rms,
            #                  get_normalized_rms(optimal_wav_data, wav_rate))

            wavfile.write("{0}-dilated-pass{1}.wav".format(output_filename, pass_idx), wav_rate,
                          stretch.normalize_rms(optimal_wav_data, reference_overall_rms))


def main(argv=None):
    program_name = os.path.basename(sys.argv[0])
    program_version = "v" + str(__version__)
    program_build_date = "%s" % __updated__

    program_version_string = '%%prog %s (%s)' % (program_version, program_build_date)
    program_longdesc = '''Vocal stitch'''
    program_license = "GPL v3+ 2016 Olivier Jolly"

    if argv is None:
        argv = sys.argv[1:]

    try:
        parser = argparse.ArgumentParser(epilog=program_longdesc,
                                         description=program_license)
        parser.add_argument("-d", "--debug", dest="debug", action="store_true",
                            default=False,
                            help="debug operations [default: %(default)s]")
        parser.add_argument("-n", dest="dry_run", action="store_true",
                            default=False,
                            help="don't perform actions [default: %(default)s]"
                            )
        parser.add_argument("wav_file", nargs="+")

        # process options
        opts = parser.parse_args(argv)

    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

    if opts.debug:
        logging.root.setLevel(logging.DEBUG)
    else:
        logging.root.setLevel(logging.INFO)

    logging.basicConfig(format="%(levelname)s:%(message)s")

    dilate_wavfiles(opts.wav_file[0], opts.wav_file[1:], **vars(opts))

    return 0


if __name__ == '__main__':
    sys.exit(main())
