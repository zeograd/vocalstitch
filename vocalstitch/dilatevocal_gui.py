import logging
import sys

import PyQt5
import os
from PyQt5.QtCore import Qt, QObjectCleanupHandler
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QVBoxLayout, QMainWindow, QWidget, QHBoxLayout, \
    QPushButton, QLabel, QFileDialog, QGridLayout, QAction, QMessageBox, QDialog

from vocalstitch.dilatevocal_gui_about import Ui_AboutDialog


class AboutDialog(QDialog, Ui_AboutDialog):
    def __init__(self):
        super(AboutDialog, self).__init__()
        self.setupUi(self)


pyqt = os.path.dirname(PyQt5.__file__)
QApplication.addLibraryPath(os.path.join(pyqt, "plugins"))
# QApplication.addLibraryPath(pyqt)

if getattr(sys, 'frozen', False):
    FullDirectory = os.path.join(os.environ.get("_MEIPASS", sys._MEIPASS))
    os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = FullDirectory
    os.chdir(FullDirectory)
else:
    # pyqt = os.path.dirname(PyQt5.__file__)
    os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = pyqt


class WavFileWidget(QWidget):
    def __init__(self):
        super(WavFileWidget, self).__init__()
        self.initUI()

    def initUI(self):
        open_btn = QPushButton('Open File')
        self.filename_lbl = QLabel()

        hbox = QHBoxLayout()
        hbox.addWidget(open_btn)
        hbox.addWidget(self.filename_lbl)

        vbox = QVBoxLayout()
        vbox.addWidget(QLabel("Target file"))
        vbox.addLayout(hbox)
        self.setLayout(vbox)

        open_btn.clicked.connect(self.showFileDialog)

    def showFileDialog(self):
        fname = QFileDialog.getOpenFileName(self, caption='Open file', filter='*.wav')

        if fname[0]:
            self.filename_lbl.setText(fname[0])


class ReferenceWavFileWidget(WavFileWidget):
    def initUI(self):
        open_btn = QPushButton('Open Reference File')
        self.filename_lbl = QLabel()

        hbox = QHBoxLayout()
        hbox.addWidget(open_btn)
        hbox.addWidget(self.filename_lbl)
        self.setLayout(hbox)

        open_btn.clicked.connect(self.showFileDialog)


class OpenFileButton(QPushButton):
    def __init__(self, file_idx):
        super(OpenFileButton, self).__init__()
        self.file_idx = file_idx
        self.setText('Open File')
        self.clicked.connect(self.showFileDialog)

    def showFileDialog(self, *args, **kwargs):
        fname = QFileDialog.getOpenFileNames(self, caption='Open file', filter='*.wav')

        if fname[0]:
            self.parent().setFiles(fname[0], self.file_idx)


class RemoveButton(QPushButton):
    def __init__(self, file_idx):
        super(RemoveButton, self).__init__()
        self.file_idx = file_idx
        self.setText('Remove')
        self.clicked.connect(self.removeFile)

    def removeFile(self):
        self.parent().removeFile(self.file_idx)


class DilateButton(QPushButton):
    def __init__(self):
        super(DilateButton, self).__init__()
        self.setText('Dilate file(s)')
        self.clicked.connect(self.dilateFiles)

    def dilateFiles(self):
        self.setDisabled(True)
        self.parent().parent().dilateFiles()
        self.setDisabled(False)


class WavFileContainerWidget(QWidget):
    def __init__(self):
        super(WavFileContainerWidget, self).__init__()
        self.filenames = []
        self.options = {}
        self.initUI()

    def initUI(self):
        self.refreshWidgets()

    def refreshWidgets(self):

        # clean previous layout
        try:
            while self.grid_layout.count():
                item = self.grid_layout.takeAt(0)
                widget = item.widget()
                if widget:
                    widget.deleteLater()
            QObjectCleanupHandler().add(self.grid_layout)
        except AttributeError:
            pass

        dilate_btn = DilateButton()

        if self.parent():
            self.parent().statusBar().showMessage('Ready to process files')

        self.grid_layout = QGridLayout()

        # add all existing target files first
        for idx, name in enumerate(self.filenames[1:]):
            self.grid_layout.addWidget(QLabel('Target file #{0}'.format(1 + idx)), 3 + idx * 2, 0, 1, 3)
            self.grid_layout.addWidget(OpenFileButton(1 + idx), 4 + idx * 2, 0)
            self.grid_layout.addWidget(QLabel(self.filenames[1 + idx]), 4 + idx * 2, 1)
            self.grid_layout.addWidget(RemoveButton(1 + idx), 4 + idx * 2, 2)

        # disable button if no targets are selected
        if not self.filenames[1:]:
            dilate_btn.setEnabled(False)
            if self.parent():
                self.parent().statusBar().showMessage('Open one or more target files')

        # add the reference file
        self.grid_layout.addWidget(QLabel('Reference file'), 1, 0, 1, 3)
        self.grid_layout.addWidget(OpenFileButton(0), 2, 0)

        try:
            self.grid_layout.addWidget(QLabel(self.filenames[0]), 2, 1)
        except IndexError:
            # disable dilate button is no reference is selected
            dilate_btn.setEnabled(False)
            if self.parent():
                self.parent().statusBar().showMessage('Open a reference file')

        # add an empty target file widget
        self.grid_layout.addWidget(QLabel('New target file'), 3 + 2 * len(self.filenames[1:]), 0, 1, 3)
        self.grid_layout.addWidget(OpenFileButton(1 + len(self.filenames[1:])), 4 + 2 * len(self.filenames[1:]), 0)

        # add the dilate button at bottom
        self.grid_layout.addWidget(dilate_btn, 5 + 2 * len(self.filenames[1:]), 0, 1, 3, Qt.AlignCenter)

        # pack and show the result
        self.setLayout(self.grid_layout)
        self.resize(self.grid_layout.sizeHint())
        self.show()

    def setFile(self, file_name, file_idx):
        if file_idx < len(self.filenames):
            self.filenames[file_idx] = file_name
        else:
            self.filenames.append(file_name)
            #  self.addTargetFileWidget()

    def setFiles(self, filenames, start_idx):
        for file_idx, file_name in zip(range(start_idx, start_idx + len(filenames)), filenames):
            self.setFile(file_name, file_idx)
        self.refreshWidgets()

    def removeFile(self, file_idx):
        self.filenames[file_idx:] = self.filenames[file_idx + 1:]
        self.refreshWidgets()


class DilateMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.wavFileWidget = WavFileContainerWidget()
        self.initUI()

    def initUI(self):
        self.setCentralWidget(self.wavFileWidget)

        self.wavFileWidget.refreshWidgets()

        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QApplication.quit)

        openReferenceAction = QAction(QIcon(''), 'Open &reference file...', self)
        openReferenceAction.setShortcut('Ctrl+O')
        openReferenceAction.setStatusTip('Open reference file')
        openReferenceAction.triggered.connect(self.openReferenceFile)

        openTargetAction = QAction(QIcon(''), 'Open &target files...', self)
        openTargetAction.setShortcut('Ctrl+T')
        openTargetAction.setStatusTip('Open target files')
        openTargetAction.triggered.connect(self.openTargetFiles)

        dilateAction = QAction('&Dilate file(s)', self)
        dilateAction.setShortcut('Ctrl+D')
        dilateAction.setStatusTip('Dilate file(s)')
        dilateAction.triggered.connect(self.dilateFiles)

        aboutQtAction = QAction('About &Qt', self)
        aboutQtAction.setStatusTip('About Qt')
        aboutQtAction.triggered.connect(self.showAboutQt)

        aboutAction = QAction('&About', self)
        aboutAction.setStatusTip('About this software')
        aboutAction.triggered.connect(self.showAbout)

        menubar = self.menuBar()

        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(openReferenceAction)
        fileMenu.addAction(openTargetAction)
        fileMenu.addSeparator()
        fileMenu.addAction(exitAction)

        menubar.addAction(dilateAction)

        helpMenu = menubar.addMenu('&Help')
        helpMenu.addAction(aboutQtAction)
        helpMenu.addAction(aboutAction)

        self.setWindowTitle('DilateVocal')
        self.show()

    def showAboutQt(self):
        QMessageBox().aboutQt(self)

    def showAbout(self):
        dialog = QDialog(self)
        dialog.ui = Ui_AboutDialog()
        dialog.ui.setupUi(dialog)
        dialog.exec()

    def dilateFiles(self):
        from vocalstitch import dilatevocal
        try:
            self.statusBar().showMessage('Please wait. Processing files...')
            dilatevocal.dilate_wavfiles(self.wavFileWidget.filenames[0], self.wavFileWidget.filenames[1:],
                                        **self.wavFileWidget.options)
            self.statusBar().showMessage('Processing done')
        except IndexError:
            pass
        finally:
            self.setDisabled(False)

    def openFile(self, caption, idx):
        fname = QFileDialog.getOpenFileNames(self, caption=caption, filter='*.wav')

        if fname[0]:
            self.wavFileWidget.setFiles(fname[0], idx)

    def openReferenceFile(self):
        self.openFile('Open reference file', 0)

    def openTargetFiles(self):
        self.openFile('Open target files', 1)


def main():
    logging.root.setLevel(logging.INFO)
    app = QApplication(sys.argv)
    ex = DilateMainWindow()
    return app.exec_()

if __name__ == '__main__':
    sys.exit(main())
