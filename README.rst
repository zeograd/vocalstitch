VocalStitch
===========

Installation
------------




License
-------

This software suite is licensed under GPL version 3 or any later version

Acknowledgement
---------------

This software suite relies on the GPL version of Rubber Band for time-stretching.
Rubber band is written by Chris Cannam, chris.cannam@breakfastquay.com.
Copyright 2007-2012 Particular Programs Ltd.
Its sources can be found on http://code.breakfastquay.com/projects/rubberband/
