# -*- mode: python -*-

block_cipher = None

import os.path
import PyQt5

a = Analysis(['vocalstitch\\dilatevocal_gui.py'],
             pathex=['.'],
             binaries=None,
             datas=[(os.path.join(os.path.dirname(PyQt5.__file__), 'plugins', 'platforms', 'qwindows.dll'), '.'),
			 (os.path.join('rubberband', 'msvcr100.dll'), '.'),
			 (os.path.join('rubberband', 'libsndfile-1.dll'), '.')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

a.datas +=  [ ('rubberband.exe', os.path.join('rubberband', 'rubberband-win.exe'), 'DATA'), ]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='dilatevocal_gui',
          debug=False,
          strip=False,
          upx=True,
          console=True )
